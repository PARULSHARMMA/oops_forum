<?php
   require('./src/autoloader.php');

   autoloader(array(array(
      'debug' => true, // turn on debug mode (by default debug mode is off)
      'basepath' => '/Applications/MAMP/htdocs/forum', // basepath is used to define where your project is located
      'extensions' => array('.php'), // allowed class file extensions
      // 'extensions' => array('.php', '.php4', '.php5'), // example of multiple extensions
   )));


   autoloader(array(
      'src'
   ));

   ini_set("display_errors","On");
   error_reporting(E_ALL);
?>



<!doctype html>
<html lang="en-US">
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <title>forum data</title>
  <meta name="author" content="Jake Rocheleau">
  <link rel="shortcut icon" href="http://www.webdesignerdepot.com/favicon.ico">
  <link rel="icon" href="http://www.webdesignerdepot.com/favicon.ico">
  <link rel="stylesheet" type="text/css" href="css/styles.css">
  <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Wellfleet">
<!--[if lt IE 9]>
  <script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>

<body>


<?php

  if(isset($_GET['id']))
  {
  $connection = new Connection("root","root","forum_schema","localhost");
  $content = new Content($connection);    $id=$_GET['id'];
    $data = $content->getData($id);
    $comment_body = $content->getComments($id);
    $get_cat = $content->getCategory($id);
    $get_tag = $content->getTags($id);
    $tags = '';
    foreach ($get_tag as $value) {
      $tags .= $value[0] . ' ';
    }
    $categories = '';
    foreach ($get_cat as $value) {
      $categories .= $value[0] . ' ';
    }
    echo "<p style='padding:18px 130px; background:rgba(1, 4, 58,0.8); text-decoration:none; color:white;display:block; font-size:24px;'>Topic : $data[0]</p>
        <p id ='id1' style='padding:10px 10px 30px 1075px; text-decoration:none; display:block;'><span style='padding:10px; background:rgba(1, 4, 58,0.8); color:white;  border-radius:2px;'> Tags : $tags </span></p>
      <p id ='created' style='padding:10px 10px 52px 100px;  text-decoration:none; display:block;'>Date : <span style='padding:10px; background:rgba(1, 4, 58,0.8);  border-radius:2px;'>" .date_format(date_create($data[2]),'Y m, d'). "</span></p>
         <p id ='category' style='padding:0px 10px 30px 1000px; margin-top:-53px; text-decoration:none; display:block;'>Categories : <span style='padding:10px; background:rgba(1, 4, 58,0.8);  border-radius:2px;'> $categories </span></p>
         <p style='padding:18px 33px 30px 130px; background:rgba(1, 4, 58,0.8); color:white; text-decoration:none; display:block;'>Description : $data[1]</p>
         <p id ='id1' style='padding:38px 28px 27px 133px; text-decoration:none; display:block;'><span style='padding:10px; background:rgba(1, 4, 58,0.8); color:white; display:inline; border-radius:2px;'>Author : $data[3]</span></p>";
    echo "<div id='w'>
    <h1>Comments Block</h1>

    <div id='container'>
      <ul id='comments'>";
        foreach ($comment_body as $comment){
            echo "<li class='cmmnt'>
              <div class='avatar'><a href='javascript:void(0);'><img src='images/dark-cubes.png' width='55' height='55' alt=$comment[2]></a></div>
              <div class='cmmnt-content'>
                <header><a href='javascript:void(0);' class='userlink'>$comment[2]</a> - <span class='pubdate'>$comment[1]</span></header>
                <p>$comment[0]</p>
      </div>
                </li>";
            }
  echo "</ul></li>";

}
?>
</body>
</html>
