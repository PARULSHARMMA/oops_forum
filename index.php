<?php
   require('./src/autoloader.php');

   autoloader(array(array(
      'debug' => true, // turn on debug mode (by default debug mode is off)
      'basepath' => '/Applications/MAMP/htdocs/forum', // basepath is used to define where your project is located
      'extensions' => array('.php'), // allowed class file extensions
      // 'extensions' => array('.php', '.php4', '.php5'), // example of multiple extensions
   )));


   autoloader(array(
      'src'
   ));

   ini_set("display_errors","On");
   error_reporting(E_ALL);
   ?>
<!DOCTYPE html>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <title>fasco</title>
      <link rel="stylesheet" href="css/first.css">
      <link rel="shortcut icon" href="http://www.webdesignerdepot.com/favicon.ico">
   </head>
   <body>
      <div class="content-wrapper">
         <div class="header">
           <a class ="login-button" href="login.html"><span>Login</span></span></a>
           <a id ="sign-up-button" href="login.html">Sign Up</a>
         </div>
         <div class="first" style="color:rgba(50, 91, 158,0.8);">
            <?php
               $connection = new Connection("root","root","forum_schema","localhost");
               $content = new Content($connection);
               $latest_fids = $content->getLatest(5);
               foreach ($latest_fids as $fid) {
                 $row = $content->read($fid);
                 echo "<a style='padding:18px 10px; text-decoration:none; display:block;' href='forum-data.php?id=$row[0]'>" . $row[1] . "</a>";
               }

               ?>
         </div>
         <a id ="addq" href="add.html">Add Question</a>
         <a id ="usrcr" href="userCreate.php">Create User</a>
   </body>
</html>
