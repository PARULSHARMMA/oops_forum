<?php
class Content implements BaseInterface
{

  public $conn;

  public function __construct(Connection $conn){
    $this->conn = $conn->connect();
  }

  public function read($id){
    $query = "select * from forum where fid=$id";
    $result = mysqli_query($this->conn,$query);
    $result = mysqli_fetch_all($result);
    return $result[0];

  }

  public function displayComment(){
    $query = "select body from comments";
    $result = mysqli_query($this->conn, $query);
    $result = mysqli_fetch_all($result);
    $body = array();
    foreach ($result as $row) {
      $body[] = $row[0];
    }
    return $body;
  }

  public function create($id){
    $query = "select * from forum where fid=$fid";
    $result = mysqli_connect($this->conn,$query);
  }


  public function update($id){
    $query = "select * from forum where fid=$fid";
    $result = mysqli_connect($this->conn,$query);

  }


  public function delete($id){
    $query = "select * from forum where fid=$fid";
    $result = mysqli_connect($this->conn,$query);
  }

  public function getLatest($num){
    $query = "select fid from forum ORDER BY created DESC LIMIT $num";
    $result = mysqli_query($this->conn, $query);
    $result = mysqli_fetch_all($result);
    $latest = array();
    foreach ($result as $row) {
      $latest[] = $row[0];
    }
    return $latest;
  }


  public function getData($id){
    $query = "SELECT title, description, created, username from forum join user on user.uid = forum.author where fid = $id";
    $result = mysqli_query($this->conn,$query);
    $result = mysqli_fetch_all($result);
    return $result[0];
  }

  public function getComments($id) {
    $query = "SELECT body, date, username from comments join user on user.uid = comments.author  where fid = $id";
    $result = mysqli_query($this->conn,$query);
    $result = mysqli_fetch_all($result);
    return $result;
  }


  public function getCategory($id){
    $query = "SELECT name from content_categories join categories on categories.cid=content_categories.cid where fid = $id";
    $result = mysqli_query($this->conn,$query);
    $result = mysqli_fetch_all($result);
    return $result;
  }


  public function getTags($id){
    $query = "SELECT name from content_tags join tags on tags.tid=content_tags.tid where fid = $id";
    $result = mysqli_query($this->conn,$query);
    $result = mysqli_fetch_all($result);
    return $result;
  }

}

?>
