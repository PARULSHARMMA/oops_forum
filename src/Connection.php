<?php

class Connection{
  private $user;
  private $pass;
  private $db;
  private $host;
  public function __construct($user, $pass, $db, $host) {
    $this->user = $user;
    $this->pass = $pass;
    $this->db = $db;
    $this->host = $host;
  }

  public function connect() {
    $link = mysqli_init();
    $conn = mysqli_connect($this->host, $this->user, $this->pass, $this->db);
    mysqli_select_db($conn, $this->db);
    if(!$conn){
      die('Unable to connect to database.');
      return NULL;
    }
    return $conn;
  }
}

?>
